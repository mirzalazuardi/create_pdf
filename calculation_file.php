<!DOCTYPE html>
<html lang="en">
<?php
require_once 'core.php';
$class = new Core;
?>

<head>
    <meta charset="utf-8" />
    <title>Calculation | Intertek</title>
    <!-- Bootstrap Css -->
    <link rel="stylesheet" href="assets/bootstrap.min.css">
    <style>
        body {
            font-size: 12px;
            color: #000000;
        }

        .td {
            vertical-align: middle;
        }

        .bordered {
            width: 100%;
            font-family: Arial, Helvetica, sans-serif;
        }

        .bordered td,
        .bordered th {
            border: 1px solid #000000;
            padding-left: 2px;
            padding-right: 2px;
        }

        .bordered tr:nth-child(even) {
            background-color: #ffffff;
        }

        .bordered tr:hover {
            background-color: #ffffff;
        }

        .bordered th {
            padding-top: 2px;
            padding-bottom: 2px;
            text-align: left;
        }
    </style>
</head>

<body class="bg-white">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <!-- Title -->
                <table style="width: 100%">
                    <thead>
                        <tr>
                            <th><img src="assets/images/logo-dark.jpg" width="80"></th>
                        </tr>
                    </thead>
                </table>

                <!-- Identity -->
                <table align="center" style="width: 100%">
                    <thead>
                        <tr>
                            <th class="text-center py-3 pb-4 font-14" colspan="5">
                                Result
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>

                            <td>Sampling Purpose</td>
                            <td class="border-bottom"><?= $_GET['id'] ?></td>

                            <td width="50"></td>

                            <td>Date</td>
                            <td class="border-bottom"><?= $_GET['id6'] ?></td>

                        </tr>
                        <tr>

                            <td>Scope of Work</td>
                            <td class="border-bottom"><?= $_GET['id2'] ?></td>

                            <td width="50"></td>

                            <td>Time</td>
                            <td class="border-bottom"><?= $_GET['id7'] ?></td>

                        </tr>
                        <tr>

                            <td>Plant Name</td>
                            <td class="border-bottom"><?= $_GET['id3'] ?></td>

                            <td width="50"></td>

                            <td>Operator</td>
                            <td class="border-bottom"><?= $_GET['id8'] ?></td>

                        </tr>
                        <tr>

                            <td>Sampling Location</td>
                            <td class="border-bottom"><?= $_GET['id4'] ?></td>

                            <td width="50"></td>

                            <td></td>
                            <td class="border-bottom"></td>

                        </tr>
                        <tr>

                            <td>Stack Type</td>
                            <td class="border-bottom"><?= $_GET['id5'] ?></td>

                            <td width="50"></td>

                            <td></td>
                            <td class="border-bottom"></td>

                        </tr>
                    </tbody>
                </table>

                <!-- Summary Data -->
                <table class="bordered my-3 text-center" style="vertical-align: middle">
                    <tbody>
                        <tr>
                            <th>Summary Data</th>
                        </tr>
                    </tbody>
                </table>

                <table class="bordered">
                    <tbody>
                        <tr>
                            <th>θ</td>
                            <td>Run time</td>
                            <td class="text-right bg-primary"><?= $_GET['id9'] ?></td>
                            <td>Minutes</td>
                            <td class="border-0"> </td>
                            <td> </td>
                            <td>Stack Diameter</th>
                            <td class="text-right bg-primary"><?= $_GET['id18'] ?></td>
                            <td>m</td>
                        </tr>
                        <tr>
                            <th>Dnz</th>
                            <td>Nozzle Diameter</td>
                            <td class="text-right bg-primary"><?= $_GET['id10'] ?></td>
                            <td>mm</td>
                            <td class="border-0"> </td>
                            <th>Pb</th>
                            <td>Baro. Pressure</td>
                            <td class="text-right bg-primary"><?= $_GET['id19'] ?></td>
                            <td>mmHg</td>
                        </tr>
                        <tr>
                            <th>Cp</th>
                            <td>Pitot Tube Coeficient</td>
                            <td class="text-right bg-primary"><?= $_GET['id11'] ?></td>
                            <td> </td>
                            <td class="border-0"> </td>
                            <th>Pg</th>
                            <td>Static Pressure</td>
                            <td class="text-right bg-primary"><?= $_GET['id20'] ?></td>
                            <td>mmHg</td>
                        </tr>
                        <tr>
                            <th>Y</th>
                            <td>Dry Gas Meter Calibration Factor</td>
                            <td class="text-right bg-primary"><?= $_GET['id12'] ?></td>
                            <td> </td>
                            <td class="border-0"> </td>
                            <th>Ps</th>
                            <td>Absolute Stack Pressure</td>
                            <td class="text-right bg-primary"><?= $_GET['id21'] ?></td>
                            <td>mmHg</td>
                        </tr>
                        <tr>
                            <th>@∆H </th>
                            <td>Average orifice meter Differential</td>
                            <td class="text-right bg-primary"><?= $_GET['id13'] ?></td>
                            <td> </td>
                            <td class="border-0"> </td>
                            <th>∆H </th>
                            <td>Avg Orrifice Pressure Drop</td>
                            <td class="text-right bg-primary"><?= $_GET['id22'] ?></td>
                            <td>mmH2O</td>
                        </tr>
                        <tr>
                            <th>Vm</th>
                            <td>DGM Volume</td>
                            <td class="text-right bg-primary"><?= $_GET['id14'] ?></td>
                            <td>m3</td>
                            <td class="border-0"> </td>
                            <th>∆P </th>
                            <td>Avg SQRT Pitot Pressure</td>
                            <td class="text-right bg-primary"><?= $_GET['id23'] ?></td>
                            <td>mmH2O</td>
                        </tr>
                        <tr>
                            <th>Tm</th>
                            <td>Average Meter Temp</td>
                            <td class="text-right bg-primary"><?= $_GET['id15'] ?></td>
                            <td>oC</td>
                            <td class="border-0"> </td>
                            <td> </td>
                            <td>Total Water Volume collected</td>
                            <td class="text-right bg-primary"><?= $_GET['id24'] ?></td>
                            <td>ml</td>
                        </tr>
                        <tr>
                            <th>Ts</th>
                            <td>Average Stack Temp</td>
                            <td class="text-right bg-primary"><?= $_GET['id16'] ?></td>
                            <td>oC</td>
                            <td class="border-0"> </td>
                            <th>Md</th>
                            <td>Dry Molecular weight</td>
                            <td class="text-right bg-primary"><?= $_GET['id25'] ?></td>
                            <td>g/g-mole</td>
                        </tr>
                        <tr>
                            <th>O2</th>
                            <td>Oxygen</td>
                            <td class="text-right bg-primary"><?= $_GET['id17'] ?></td>
                            <td>%</td>
                            <td class="border-0"></td>
                            <td class="border-0"></td>
                            <td class="border-0"></td>
                            <td class="border-0"></td>
                            <td class="border-0"></td>
                        </tr>
                    </tbody>
                </table>

                <!-- Calculated Data -->
                <table class="bordered my-3 text-center" style="vertical-align: middle">
                    <tbody>
                        <tr>
                            <th>Calculated Data</th>
                        </tr>
                    </tbody>
                </table>

                <table class="bordered" style="vertical-align: middle">
                    <thead>
                        <tr>
                            <th class="border-0" colspan="4">1. Mouisture Content</th>
                            <td class="border-0"> </td>
                            <th class="border-0" colspan="4">2. Volumetric Flow Rate Data</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>Vwc</th>
                            <td>Standard Water Volume Vapor Volume</td>
                            <td class="text-right bg-danger"><?= $_GET['id26'] ?></td>
                            <td>m3</td>
                            <td class="border-0"> </td>
                            <th>Vs</th>
                            <td>Avg Stack Gas Velocity</td>
                            <td class="text-right bg-danger"><?= $_GET['id31'] ?></td>
                            <td>m/s</td>
                        </tr>
                        <tr>
                            <th>Vmstd</th>
                            <td>Standard Meter Volume</td>
                            <td class="text-right bg-danger"><?= $_GET['id27'] ?></td>
                            <td>m3</td>
                            <td class="border-0"> </td>
                            <th>An</th>
                            <td>Nozzle Cross Sectional Area</td>
                            <td class="text-right bg-danger"><?= $_GET['id32'] ?></td>
                            <td>m2</td>
                        </tr>
                        <tr>
                            <th>Bws</th>
                            <td>mole fraction of water vapor</td>
                            <td class="text-right bg-danger"><?= $_GET['id28'] ?></td>
                            <td> </td>
                            <td class="border-0"> </td>
                            <th>Qaw</th>
                            <td>Actual Stack Flowrate</td>
                            <td class="text-right bg-danger"><?= $_GET['id33'] ?></td>
                            <td>m3/min</td>
                        </tr>
                        <tr>
                            <td> </td>
                            <td>Percent Mouisture</td>
                            <td class="text-right bg-danger"><?= $_GET['id29'] ?></td>
                            <td>%</td>
                            <td class="border-0"> </td>
                            <th>Qsd</th>
                            <td>Dry Standard Stack Flowrate</td>
                            <td class="text-right bg-danger"><?= $_GET['id34'] ?></td>
                            <td>m3/min</td>
                        </tr>
                        <tr>
                            <th>Ms</th>
                            <td>Wet Molecular Weight</td>
                            <td class="text-right bg-danger"><?= $_GET['id30'] ?></td>
                            <td>g/g-mole</td>
                            <td class="border-0"> </td>
                            <th>I</th>
                            <td>Percent Of Isokinetic Rate</td>
                            <td class="text-right bg-danger"><?= $_GET['id35'] ?></td>
                            <td>%</td>
                        </tr>
                    </tbody>
                    <thead>
                        <tr>
                            <th class="border-0 pt-2" colspan="4">3. Pollutan Data</th>
                            <td class="border-0"> </td>
                            <th class="border-0" colspan="4"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>W</th>
                            <td>Particulate weight</td>
                            <td class="text-right bg-danger"><?= $_GET['id36'] ?></td>
                            <td>mg</td>
                            <td class="border-0"> </td>
                            <td class="border-0"></td>
                            <td class="border-0"></td>
                            <td class="border-0"></td>
                            <td class="border-0"></td>
                        </tr>
                        <tr>
                            <th>C</th>
                            <td>Particulate Concentration</td>
                            <td class="text-right bg-danger"><?= $_GET['id37'] ?></td>
                            <td>mg/m3</td>
                            <td class="border-0"> </td>
                            <td class="border-0"></td>
                            <td class="border-0"></td>
                            <td class="border-0"></td>
                            <td class="border-0"></td>
                        </tr>
                        <tr>
                            <th>PMR</th>
                            <td>Pollutan Mass Rate</td>
                            <td class="text-right bg-danger"><?= $_GET['id38'] ?></td>
                            <td>kg/h</td>
                            <td class="border-0"> </td>
                            <td class="border-0"></td>
                            <td class="border-0"></td>
                            <td class="border-0"></td>
                            <td class="border-0"></td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</body>

</html>