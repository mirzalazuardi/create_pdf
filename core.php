<?php
class Core
{
    function base_url()
    {
        $link = "http://localhost/PDF_Intertek/";
        return $link;
    }
    function random_digit($min, $max)
    {
        $float_part = mt_rand(0, mt_getrandmax()) / mt_getrandmax();
        $integer_part = mt_rand($min, $max - 1);
        $mix = $integer_part + $float_part;
        $res = vprintf("%d", sscanf($mix, "%3d"));
        return $res;
    }
}
