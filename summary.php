<?php
require_once 'dompdf/autoload.inc.php';
require_once 'dompdf/dompdf_config.inc.php';
require_once 'core.php';

use Dompdf\Dompdf;

class PDF extends Core
{
    public function Generate($id, $id2, $id3, $id4, $id5, $id6, $id7, $id8, $id9, $id10, $id11, $id12, $id13, $id14, $id15, $id16, $id17, $id18, $id19, $id20, $id21, $id22, $id23, $id24, $id25, $id26, $id27, $id28, $id29)
    {
        echo 'Please wait...' . PHP_EOL;

        $file = file_get_contents(
            $this->base_url() .
                'summary_file.php?id=' . $id .
                '&id2=' . $id2 .
                '&id3=' . $id3 .
                '&id4=' . $id4 .
                '&id5=' . $id5 .
                '&id6=' . $id6 .
                '&id7=' . $id7 .
                '&id8=' . $id8 .
                '&id9=' . $id9 .
                '&id10=' . $id10 .
                '&id11=' . $id11 .
                '&id12=' . $id12 .
                '&id13=' . $id13 .
                '&id14=' . $id14 .
                '&id15=' . $id15 .
                '&id16=' . $id16 .
                '&id17=' . $id17 .
                '&id18=' . $id18 .
                '&id19=' . $id19 .
                '&id20=' . $id20 .
                '&id21=' . $id21 .
                '&id22=' . $id22 .
                '&id23=' . $id23 .
                '&id24=' . $id24 .
                '&id25=' . $id25 .
                '&id26=' . $id26 .
                '&id27=' . $id27 .
                '&id28=' . $id28 .
                '&id29=' . $id29
        );

        $dompdf = new Dompdf();
        $dompdf->loadHtml($file);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $output = $dompdf->output();
        $filename = 'assets/pdf/summary/' . $this->DateTime() . '_summary.pdf';
        file_put_contents($filename, $output);

        echo 'PDF Successfully created ';
        echo $this->base_url() . $filename;
    }
    public function DateTime()
    {
        return date('Y_m_d_H_i_s');
    }
}

// Disable Error
error_reporting(0);

// Check Parameter
if (!$_SERVER["argv"][1]) {
    echo "No Parameter";
} else {
    $cPDF = new PDF;
    $cPDF->Generate(
        $_SERVER["argv"][1],
        $_SERVER["argv"][2],
        $_SERVER["argv"][3],
        $_SERVER["argv"][4],
        $_SERVER["argv"][5],
        $_SERVER["argv"][6],
        $_SERVER["argv"][7],
        $_SERVER["argv"][8],
        $_SERVER["argv"][9],
        $_SERVER["argv"][10],
        $_SERVER["argv"][11],
        $_SERVER["argv"][12],
        $_SERVER["argv"][13],
        $_SERVER["argv"][14],
        $_SERVER["argv"][15],
        $_SERVER["argv"][16],
        $_SERVER["argv"][17],
        $_SERVER["argv"][18],
        $_SERVER["argv"][19],
        $_SERVER["argv"][20],
        $_SERVER["argv"][21],
        $_SERVER["argv"][22],
        $_SERVER["argv"][23],
        $_SERVER["argv"][24],
        $_SERVER["argv"][25],
        $_SERVER["argv"][26],
        $_SERVER["argv"][27],
        $_SERVER["argv"][28],
        $_SERVER["argv"][29]
    );
}
