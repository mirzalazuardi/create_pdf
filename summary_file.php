<!DOCTYPE html>
<html lang="en">
<?php
require_once 'core.php';
$class = new Core;
//$data = $class->SummaryData();
?>

<head>
    <meta charset="utf-8" />
    <title>Summary | Intertek</title>
    <!-- Bootstrap Css -->
    <link rel="stylesheet" href="assets/bootstrap.min.css">
    <style>
        body {
            font-size: 12px;
            color: #000000;
        }

        .td {
            vertical-align: middle;
        }

        .bordered {
            width: 100%;
        }

        .bordered td,
        .bordered th {
            border: 1px solid #000000;
            padding: 1px;
        }

        .bordered tr:nth-child(even) {
            background-color: #ffffff;
        }

        .bordered tr:hover {
            background-color: #ffffff;
        }

        .bordered th {
            padding-top: 5px;
            padding-bottom: 5px;
            text-align: left;
        }

        .tr-data td:nth-child(4) {
            border: 1px solid #000000;
            padding: 1px;
            text-align: center;
        }

        .tr-data td:nth-child(5) {
            border: 1px solid #000000;
            padding: 1px;
            text-align: center;
        }
    </style>
</head>

<body class="bg-white">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <!-- Title -->
                <table style="width: 100%">
                    <thead>
                        <tr>
                            <th><img src="assets/images/logo-dark.jpg" width="80"></th>
                        </tr>
                    </thead>
                </table>

                <table align="center" style="width: 100%">
                    <thead>
                        <tr>
                            <th class="text-center py-3 pb-4 font-14" colspan="2">
                                Summary
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td width="100">Scope Of Work</td>
                            <td width="100" class="border-bottom"> <?= $_GET['id'] ?></td>

                        </tr>
                        <tr>

                            <td>Plant Name</td>
                            <td class="border-bottom"><?= $_GET['id2'] ?></td>

                        </tr>
                        <tr>

                            <td>Sampling Location</td>
                            <td class="border-bottom"><?= $_GET['id3'] ?></td>

                        </tr>
                        <tr>

                            <td>Stack Type</td>
                            <td class="border-bottom"><?= $_GET['id4'] ?></td>

                        </tr>
                        <tr>

                            <td>Date</td>
                            <td class="border-bottom"><?= $_GET['id5'] ?></td>

                        </tr>
                    </tbody>
                </table>

                <table class="bordered my-3" style="vertical-align: middle">
                    <tbody>
                        <tr>
                            <th>Oxygen Correction</th>
                            <th class="text-center">%</th>
                            <th class="text-center"><?= $_GET['id6'] ?></th>
                        </tr>
                    </tbody>
                </table>

                <table style="width: 100%">
                    <tr class="tr-data">
                        <th>1</th>
                        <td>Run Time </td>
                        <td>Minutes</td>
                        <td><?= $_GET['id7'] ?></td>
                    </tr>
                    <tr class="tr-data">
                        <th>2</th>
                        <td>Particulates Traverse Points </td>
                        <td>Points</td>
                        <td><?= $_GET['id8'] ?></td>
                    </tr>
                    <tr class="tr-data">
                        <th>3</th>
                        <td>Velocity Traverse Points </td>
                        <td>Points</td>
                        <td><?= $_GET['id9'] ?></td>
                    </tr>
                    <tr class="tr-data">
                        <th>4</th>
                        <td>Absolute Stack Pressure</td>
                        <td>mmHg</td>
                        <td><?= $_GET['id10'] ?></td>
                    </tr>
                    <tr class="tr-data">
                        <th>5</th>
                        <td>Avg SQRT Pitot Pressure</td>
                        <td>mm.H2O</td>
                        <td><?= $_GET['id11'] ?></td>
                    </tr>
                    <tr class="tr-data">
                        <th>6</th>
                        <td>Avg SQRT Oriffice Pressure</td>
                        <td>mm.H2O</td>
                        <td><?= $_GET['id12'] ?></td>
                    </tr>
                    <tr class="tr-data">
                        <th>7</th>
                        <td>Stack Temperature</td>
                        <td>oC</td>
                        <td><?= $_GET['id13'] ?></td>
                    </tr>
                    <tr class="tr-data">
                        <th>8</th>
                        <td>Volume of Air Pumped</td>
                        <td>m3</td>
                        <td><?= $_GET['id14'] ?></td>
                    </tr>
                    <tr class="tr-data">
                        <th>9</th>
                        <td>Mole Fraction of Water Vapour</td>
                        <td>-</td>
                        <td><?= $_GET['id15'] ?></td>
                    </tr>
                    <tr class="tr-data">
                        <th>10</th>
                        <td>Percent Moisture</td>
                        <td>%</td>
                        <td><?= $_GET['id16'] ?></td>
                    </tr>
                    <tr class="tr-data">
                        <th>11</th>
                        <td>Oxygen, O2</td>
                        <td>%</td>
                        <td><?= $_GET['id17'] ?></td>
                    </tr>
                    <tr class="tr-data">
                        <th>12</th>
                        <td>Carbon Dioxides, CO2</td>
                        <td>%</td>
                        <td><?= $_GET['id18'] ?></td>
                    </tr>
                    <tr class="tr-data">
                        <th>13</th>
                        <td>Velocity</td>
                        <td>m/s</td>
                        <td><?= $_GET['id19'] ?></td>
                    </tr>
                    <tr class="tr-data">
                        <th>14</th>
                        <td>Dry Molecular Weight</td>
                        <td>g/mol</td>
                        <td><?= $_GET['id20'] ?></td>
                    </tr>
                    <tr class="tr-data">
                        <th>15</th>
                        <td>Wet Molecular Weight</td>
                        <td>g/mol</td>
                        <td><?= $_GET['id21'] ?></td>
                    </tr>
                    <tr class="tr-data">
                        <th>16</th>
                        <td>Standard Meter Volume</td>
                        <td>Dscm</td>
                        <td><?= $_GET['id22'] ?></td>
                    </tr>
                    <tr class="tr-data">
                        <th>17</th>
                        <td>Actual Stack Flowrate</td>
                        <td>m3/min</td>
                        <td><?= $_GET['id23'] ?></td>
                    </tr>
                    <tr class="tr-data">
                        <th>18</th>
                        <td>Dry Standard Stack Flowrate</td>
                        <td>m3/min</td>
                        <td><?= $_GET['id24'] ?></td>
                    </tr>
                    <tr class="tr-data">
                        <th>19</th>
                        <td>Particulate Weight</td>
                        <td>mg</td>
                        <td><?= $_GET['id25'] ?></td>
                    </tr>
                    <tr class="tr-data">
                        <th>20</th>
                        <td>Pollutant Mass Rate</td>
                        <td>kg/hour</td>
                        <td><?= $_GET['id26'] ?></td>
                    </tr>
                    <tr class="tr-data">
                        <th>21</th>
                        <td>Percent of Isokinetic</td>
                        <td>%</td>
                        <td><?= $_GET['id27'] ?></td>
                    </tr>

                    <!-- Actual -->
                    <tr>
                        <td height="20" class="border-0" colspan="4"></td>
                    </tr>
                    <tr>
                        <td class="border-0"></td>
                        <th class="border-0" colspan="3">Actual</th>
                    </tr>
                    <tr class="tr-data">
                        <th class="border-0">1</th>
                        <td class="border-0">Total particulate Matter</td>
                        <td class="border-0"></td>
                        <td class="text-center"><?= $_GET['id28'] ?></td>
                    </tr>

                    <!-- Corrected to 10% Oxygen -->
                    <tr>
                        <td height="20" class="border-0" colspan="4"></td>
                    </tr>
                    <tr>
                        <td class="border-0"></td>
                        <th class="border-0" colspan="3">Corrected to 10% Oxygen</th>
                    </tr>
                    <tr class="tr-data">
                        <th class="border-0">1</th>
                        <td class="border-0">Total particulate Matter</td>
                        <td class="border-0"></td>
                        <td class="text-center"><?= $_GET['id29'] ?></td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</body>

</html>